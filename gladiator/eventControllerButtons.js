/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { GObject, Gtk } = imports.gi;
import { FunctionMenuButton } from './menuButton.js';

// Maintain a list of custom event controllers.
Gtk.EventController.eventControllers = new Set();
Gtk.Widget.prototype.addControllerOld = Gtk.Widget.prototype.add_controller;
Gtk.Widget.prototype.add_controller = function(controller) {
    this.addControllerOld(controller);
    Gtk.EventController.eventControllers.add(controller);
};
Gtk.Widget.prototype.removeControllerOld = Gtk.Widget.prototype.remove_controller;
Gtk.Widget.prototype.remove_controller = function(controller) {
    this.removeControllerOld(controller);
    Gtk.EventController.eventControllers.delete(controller);
};
Gtk.Widget.prototype.getEventControllers = function() {
    return [...Gtk.EventController.eventControllers]
        .filter(controller => controller.widget == this);
};

export default GObject.registerClass({
    Properties: {
        'inspected-object': GObject.ParamSpec.object(
            'inspected-object', "Inspected object", "The inspected widget to manage the controllers for",
            GObject.ParamFlags.READWRITE, GObject.TYPE_OBJECT
        ),
    },
}, class extends Gtk.Box {
    _init(params) {
        super._init(Object.assign({
            spacing: 6,
        }, params));

        this._removeButton = new FunctionMenuButton({
            iconName: 'list-remove-symbolic',
            tooltipText: "Remove a controller",
        });
        this._removeButton.popover.position = Gtk.PositionType.RIGHT;
        this._removeButton.set_create_popup_func(menuButton => {
            menuButton.setParams([
                { name: "controller", instances: this.inspectedObject.getEventControllers() },
            ]);
        });
        this._removeButton.connect('apply', (b_, f_, [controller]) => {
            this.inspectedObject.remove_controller(controller);
            this._update();
        });
        this.append(this._removeButton);

        this._addButton = new FunctionMenuButton({
            iconName: 'list-add-symbolic',
            tooltipText: "Add a controller",
        });
        this._addButton.popover.position = Gtk.PositionType.RIGHT;
        this._addButton.set_create_popup_func(menuButton => {
            menuButton.setParams([
                { name: "controller", types: ['GtkEventController'] },
                { name: "name", text: true },
            ]);
        });
        this._addButton.connect('apply', (b_, f_, [controller, name]) => {
            controller.name = name ?? null;
            this.inspectedObject.add_controller(controller);
            this._update();
        });
        this.append(this._addButton);
    }

    _update() {
        this._removeButton.sensitive = !!this.inspectedObject?.getEventControllers().length;
        this._addButton.sensitive = !!this.inspectedObject;
    }

    get inspectedObject() {
        return this._inspectedObject ?? null;
    }

    set inspectedObject(inspectedObject) {
        if (!(inspectedObject instanceof Gtk.Widget))
            inspectedObject = null;

        if (this.inspectedObject == inspectedObject)
            return;

        this._inspectedObject = inspectedObject;
        this.notify('inspected-object');

        this._update();
    }
});
