/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GObject, Gtk } = imports.gi;
const ByteArray = imports.byteArray;
const Gettext = imports.gettext;
import { getPrototype } from './classes.js';

let Adw;
try {
    Adw = imports.gi.Adw;
} catch(e) {}

function instanceIsA(instance, ...typeNames) {
    return typeNames.some(typeName => (
        GObject.type_from_name(typeName) &&
        GObject.type_is_a(instance.constructor.$gtype, GObject.type_from_name(typeName))
    ));
}

const $filterCssClasses = Symbol('filterCssClasses');
const $getDefaultValue = Symbol('getDefaultValue');
const $isIdentified = Symbol('isIdentified');
const $isSkipped = Symbol('isSkipped');

GObject.Object.prototype[$filterCssClasses] = function(cssClasses) {
    return cssClasses;
};

GObject.Object.prototype[$getDefaultValue] = function(pspec) {
    return pspec.default_value;
};

GObject.Object.prototype[$isIdentified] = function(name) {
    return false;
};

GObject.Object.prototype[$isSkipped] = function(name) {
    return false;
};

let dataFile = Gio.File.new_for_uri(import.meta.url).get_parent().get_child('properties.json');
let data = JSON.parse(ByteArray.toString(dataFile.load_contents(null)[1]));

Object.entries(data).forEach(([typeName, { skip, style, toId, default: _default_ }]) => {
    let prototype = getPrototype(typeName);
    if (!prototype)
        return;

    // The properties to skip.
    if (skip) {
        // Skip comments.
        let properties = skip.filter(prop => typeof prop != 'string');

        prototype[$isSkipped] = function(name) {
            if (properties.some(prop => (
                (prop.name == name) &&
                (!prop.redundantWith || this[prop.redundantWith]) &&
                (!prop.redundantWithDiff || (this[prop.redundantWithDiff] != this[name])) &&
                (!prop.redundantWithEqual || (this[prop.redundantWithEqual] == this[name]))
            )))
                return true;

            return Object.getPrototypeOf(prototype)[$isSkipped].call(this, name);
        };
    }

    // The style classes to skip.
    if (style) {
        // Skip comments.
        let skippedStyles = style.filter(prop => typeof prop != 'string');

        prototype[$filterCssClasses] = function(cssClasses) {
            return Object.getPrototypeOf(prototype)[$filterCssClasses].call(this, cssClasses)
                .filter(cssClasse => skippedStyles.every(skippedStyle => (
                    (skippedStyle.name != cssClasse) ||
                    (skippedStyle.interfaceType && !instanceIsA(this, skippedStyle.interfaceType)) ||
                    (skippedStyle.parentType && (!this.parent || !instanceIsA(this.parent, skippedStyle.parentType))) ||
                    (skippedStyle.layoutType && (!this.parent?.layoutManager || !instanceIsA(this.parent.layoutManager, skippedStyle.layoutType)))
                )));
        };
    }

    // The properties whose value has to be replaced with a buildable ID.
    if (toId) {
        // Skip comments.
        let properties = toId.filter(prop => typeof prop != 'string');

        prototype[$isIdentified] = function(name) {
            if (properties.some(prop => (prop.name == name)))
                return true;

            return Object.getPrototypeOf(prototype)[$isIdentified].call(this, name);
        };
    }

    // The properties whose default value is overridden by a subclass.
    if (_default_) {
        // Skip comments.
        let properties = _default_.filter(prop => typeof prop != 'string');

        prototype[$getDefaultValue] = function(pspec) {
            let { domain, settingKey, value } = properties.find(prop => (prop.name == pspec.name)) ?? {};
            if (settingKey)
                // Cannot just call get_settings because of gtk_print_unix_dialog_get_settings.
                value = Gtk.Widget.prototype.get_settings.call(this)[settingKey] ?? null;
            if (domain)
                value = Gettext.domain(domain).gettext(value);

            return value ?? Object.getPrototypeOf(prototype)[$getDefaultValue].call(this, pspec);
        };
    }
});

let widgetIsSkippedOld = Gtk.Widget.prototype[$isSkipped];
Gtk.Widget.prototype[$isSkipped] = function(name) {
    if (name == 'layout-manager')
        return this[name]?.constructor.$gtype == Gtk.Widget.get_layout_manager_type.call(this.constructor);
    else if ((name == 'hadjustment' || name == 'vadjustment') && (this instanceof Gtk.Scrollable))
        return true;

    return widgetIsSkippedOld.call(this, name);
};

let widgetGetDefaultValueOld = Gtk.Widget.prototype[$getDefaultValue];
Gtk.Widget.prototype[$getDefaultValue] = function(pspec) {
    if (pspec.name == 'accessible-role')
        return Gtk.Widget.get_accessible_role.call(this.constructor);
    else if (pspec.name == 'css-name')
        return Gtk.Widget.get_css_name.call(this.constructor);
    else if (pspec.name == 'has-tooltip')
        return !!this.tooltipText;
    else if (pspec.name == 'invisible-char')
        return 0;

    return widgetGetDefaultValueOld.call(this, pspec);
};

let colorButtonIsSkippedOld = Gtk.ColorButton.prototype[$isSkipped];
Gtk.ColorButton.prototype[$isSkipped] = function(name) {
    if (name == 'rgba')
        return (
            this[name]?.red == 0 && this[name]?.green == 0 &&
            this[name]?.blue == 0 && this[name]?.alpha == 1
        );

    return colorButtonIsSkippedOld.call(this, name);
};

let iconViewIsSkippedOld = Gtk.IconView.prototype[$isSkipped];
Gtk.IconView.prototype[$isSkipped] = function(name) {
    if (name == 'cell-area')
        return this[name]?.constructor == Gtk.CellAreaBox;

    return iconViewIsSkippedOld.call(this, name);
};

let linkButtonIsSkippedOld = Gtk.LinkButton.prototype[$isSkipped];
Gtk.LinkButton.prototype[$isSkipped] = function(name) {
    if (name == 'cursor')
        return this[name]?.name == 'pointer';

    return linkButtonIsSkippedOld.call(this, name);
};

let textIsSkippedOld = Gtk.Text.prototype[$isSkipped];
Gtk.Text.prototype[$isSkipped] = function(name) {
    if (name == 'cursor')
        return this[name]?.name == 'text';

    return textIsSkippedOld.call(this, name);
};

let textViewIsSkippedOld = Gtk.TextView.prototype[$isSkipped];
Gtk.TextView.prototype[$isSkipped] = function(name) {
    if (name == 'cursor')
        return this[name]?.name == 'text';

    return textViewIsSkippedOld.call(this, name);
};

let volumeButtonGetDefaultValueOld = Gtk.VolumeButton.prototype[$getDefaultValue];
Gtk.VolumeButton.prototype[$getDefaultValue] = function(pspec) {
    if (pspec.name == 'icons')
        return this.useSymbolic ?
            ["audio-volume-muted-symbolic", "audio-volume-high-symbolic", "audio-volume-low-symbolic", "audio-volume-medium-symbolic"] :
            ["audio-volume-muted", "audio-volume-high", "audio-volume-low", "audio-volume-medium"];

    return volumeButtonGetDefaultValueOld.call(this, pspec);
};

if (Adw) {
    let comboRowGetDefaultValueOld = Adw.ComboRow.prototype[$getDefaultValue];
    Adw.ComboRow.prototype[$getDefaultValue] = function(pspec) {
        if (pspec.name == 'sensitive')
            return !!this.model?.get_n_items();

        return comboRowGetDefaultValueOld.call(this, pspec);
    };

    let tabViewIsSkippedOld = Adw.TabView.prototype[$isSkipped];
    Adw.TabView.prototype[$isSkipped] = function(name) {
        if (name == 'default-icon')
            return this[name]?.names.join() == 'adw-tab-icon-missing-symbolic';

        return tabViewIsSkippedOld.call(this, name);
    };
}

export const getCssClasses = function(object) {
    return object[$filterCssClasses](object.get_css_classes());
};

export const getDefaultValue = function(object, pspec) {
    return object[$getDefaultValue](pspec);
};

export const isIdentified = function(object, name) {
    return object[$isIdentified](name);
};

export const isSkipped = function(object, name) {
    // Skip "*-set" properties (e.g. GtkEntry:invisible-char-set).
    if (name.endsWith('-set'))
        return true;

    return object[$isSkipped](name);
};
