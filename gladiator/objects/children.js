/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GObject, Gtk } = imports.gi;
const ByteArray = imports.byteArray;
import { getPrototype } from './classes.js';
import './overrides.js';

let Adw;
try {
    Adw = imports.gi.Adw;
} catch(e) {}

const $getChild = Symbol('getChild');
const $getChildren = Symbol('getChildren');
const $removeChild = Symbol('removeChild');

const listModelToArray = function(list) {
    return Array.from({ length: list.get_n_items() }, (e_, index) => list.get_item(index));
};

function findBuildableWidget(widgets, id) {
    if (!widgets.length)
        return null;

    let buildableChild = widgets.find(widget => widget.get_buildable_id?.() == id);
    if (buildableChild)
        return buildableChild;

    return findBuildableWidget(widgets.map(widget => [...widget]).flat(), id);
}

export const isActivatable = function(widget) {
    return !!Gtk.Widget.get_activate_signal.call(widget.constructor);
};

export const searchWidgetForBuildableId = function(widgets, buildableId) {
    if (!widgets.length)
        return null;

    let buildableChild = widgets.find(widget => (widget instanceof Gtk.Buildable) && (widget.get_buildable_id() == buildableId));

    if (buildableChild)
        return buildableChild;

    return searchWidgetForBuildableId(widgets.map(widget => [...widget]).flat(), buildableId);
};

GObject.Object.prototype[$getChildren] = function() {
    return [];
};

Gtk.Widget.prototype[$getChild] = function() {
    let object = this.get_child?.();

    if (object && this.set_child)
        object[$removeChild] = () => this.set_child(null);

    return { object };
};

Gtk.Widget.prototype[$getChildren] = function() {
    return [this[$getChild]()];
};

let dataFile = Gio.File.new_for_uri(import.meta.url).get_parent().get_child('children.json');
let data = JSON.parse(ByteArray.toString(dataFile.load_contents(null)[1]));

Object.entries(data).forEach(([typeName, functions]) => {
    let prototype = getPrototype(typeName);
    if (!prototype)
        return;

    // Skip comments.
    functions = functions.filter(func => typeof func != 'string');

    prototype[$getChildren] = function() {
        let children = functions.map(func => {
            let result = this[func.name]();
            if (!result)
                return {};

            let objects = (
                (result instanceof Gio.ListModel && func.followListModel) ? listModelToArray(result) :
                Array.isArray(result) ? result :
                [result]
            );

            if (func.remover) {
                objects.forEach(object => {
                    object[$removeChild] = func.remover.isSetter ?
                        () => this[func.remover.name](null) :
                        () => this[func.remover.name](object);
                });
            }

            return objects.map(object => ({ object, element: func.element, type: func.type, ignoreUI: func.ignoreUI }));
        }).flat();

        // ⚠️: Prototype functions are not chained here.
        // return Object.getPrototypeOf(prototype)[$getChildren].call(this).concat(children);
        return children;
    };
});

Gtk.ActionBar.prototype[$getChildren] = function() {
    let centerBox = this.get_first_child().child;

    let startChildren = [...(centerBox.get_start_widget() ?? [])].map(object => ({ object, type: 'start' }));
    let centerChildren = [{ object: this.get_center_widget(), type: 'center' }];
    let endChildren = [...(centerBox.get_end_widget() ?? [])].map(object => ({ object, type: 'end' }));

    let children = startChildren.concat(centerChildren, endChildren);
    children.forEach(child => {
        if (child.object)
            child.object[$removeChild] = () => this.remove(child.object);
    });

    return children;
};

// TODO: Action widgets.
Gtk.Assistant.prototype[$getChildren] = function() {
    let list = this.get_pages();
    let pages = listModelToArray(list);

    return pages.map((page, index) => {
        page[$removeChild] = () => this.remove_page(index);
        page.child[$removeChild] = page[$removeChild];
        return [{ object: page }, { object: page.child, ignoreUI: true }];
    }).concat([{ object: list, ignoreUI: true }]).flat();
};

Gtk.Box.prototype[$getChildren] = function() {
    return [...this].map(widget => {
        widget[$removeChild] = () => this.remove(widget);
        return { object: widget };
    });
};

Gtk.Button.prototype[$getChildren] = function() {
    if (this.label && this.child instanceof Gtk.Label && this.child.label == this.label)
        return [];

    if (this.iconName && this.child instanceof Gtk.Image && this.child.iconName == this.iconName)
        return [];

    return [this[$getChild]()];
};

Gtk.ComboBox.prototype[$getChildren] = function() {
    if (this.hasEntry && (this.child?.constructor == Gtk.Entry) || (this.child?.constructor == Gtk.CellView))
        return [];

    return [this[$getChild]()];
};

Gtk.Dialog.prototype[$getChildren] = function() {
    let actionArea = searchWidgetForBuildableId([this], 'action_area');
    if (!actionArea)
        return Gtk.Window.prototype[$getChildren].call(this);

    let children = [];

    let hasContent = !!this.get_content_area().get_first_child();
    children.push({ object: this.get_content_area(), internalChild: 'content_area', ignoreUI: !hasContent });

    let actionWidgets = [...actionArea];
    children.push(...actionWidgets.map(widget => {
        widget[$removeChild] = () => actionArea.remove(widget);
        return { object: widget, type: 'action' };
    }));

    return children;
};

Gtk.Expander.prototype[$getChildren] = function() {
    let children = [this[$getChild]()];

    if (this.labelWidget && (!(this.labelWidget instanceof Gtk.Label) || this.labelWidget.label != this.label)) {
        this.labelWidget[$removeChild] = () => this.set_label_widget(null);
        children.unshift({ object: this.labelWidget, type: 'label' });
    }

    return children;
};

Gtk.Fixed.prototype[$getChildren] = function() {
    return [...this].map(widget => {
        widget[$removeChild] = () => this.remove(widget);
        return { object: widget };
    });
};

Gtk.FlowBox.prototype[$getChildren] = function() {
    return [...this].map(flowBoxChild => {
        let widget = flowBoxChild.child ?? flowBoxChild;
        widget[$removeChild] = () => this.remove(widget);
        return { object: widget };
    });
};

Gtk.Frame.prototype[$getChildren] = function() {
    return Gtk.Expander.prototype[$getChildren].call(this);
};

Gtk.Grid.prototype[$getChildren] = function() {
    return Gtk.Fixed.prototype[$getChildren].call(this);
};

Gtk.HeaderBar.prototype[$getChildren] = function() {
    let children = [];
    let centerBox = this.get_first_child().child;

    if (centerBox.get_start_widget()) {
        [...centerBox.get_start_widget()].forEach(widget => {
            if (widget instanceof Gtk.WindowControls)
                return;

            widget[$removeChild] = () => this.remove(widget);
            children.push({ object: widget, type: 'start' });
        });
    }

    if (this.get_title_widget()) {
        this.titleWidget[$removeChild] = () => this.set_title_widget(null);
        children.push({ object: this.titleWidget, type: 'title' });
    }

    if (centerBox.get_end_widget()) {
        [...centerBox.get_end_widget()].forEach(widget => {
            if (widget instanceof Gtk.WindowControls)
                return;

            widget[$removeChild] = () => this.remove(widget);
            children.push({ object: widget, type: 'end' });
        });
    }

    return children;
};

Gtk.InfoBar.prototype[$getChildren] = function() {
    let contentArea = this.get_first_child().child.get_first_child();
    let actionArea = contentArea.get_next_sibling();

    let contentChildren = [...contentArea].map(widget => {
        widget[$removeChild] = () => this.remove_child(widget);
        return { object: widget };
    });
    let actionChildren = [...actionArea].map(widget => {
        widget[$removeChild] = () => this.remove_action_widget(widget);
        return { object: widget, type: 'action' };
    });

    return contentChildren.concat(actionChildren);
};

Gtk.ListBox.prototype[$getChildren] = function() {
    // FIXME: How to retrieve the 'placeholder' child?
    return [...this].map(row => {
        let widget = row.child ?? row;
        // XXX: gtk_list_box_remove takes rows only.
        widget[$removeChild] = () => this.remove(row);
        return { object: widget };
    });
};

Gtk.Notebook.prototype[$getChildren] = function() {
    let list = this.get_pages();
    let pages = listModelToArray(list);

    return [{ object: list, ignoreUI: true }].concat(
        ['start', 'end'].map(type => {
            let widget = this.get_action_widget(Gtk.PackType[type.toUpperCase()]);
            if (widget)
                widget[$removeChild] = () => this.set_action_widget(null, Gtk.PackType[type.toUpperCase()]);

            return { object: widget, type: `action-${type}` };
        }),
        pages.map(page => {
            page[$removeChild] = () => this.remove_page(page.position);
            page.child[$removeChild] = page[$removeChild];
            return [{ object: page }, { object: page.child, ignoreUI: true }];
        })
    ).flat();
};

Gtk.Overlay.prototype[$getChildren] = function() {
    let children = [this[$getChild]()];

    [...this].forEach(widget => {
        if (widget == this.child)
            return;

        widget[$removeChild] = () => this.remove_overlay(widget);
        children.push({ object: widget, type: 'overlay' });
    });

    return children;
};

Gtk.Stack.prototype[$getChildren] = function() {
    let list = this.get_pages();
    let pages = listModelToArray(list);

    return pages.map(page => {
        page[$removeChild] = () => this.remove(page.child);
        page.child[$removeChild] = page[$removeChild];
        return [{ object: page }, { object: page.child, ignoreUI: true }];
    }).concat([{ object: list, ignoreUI: true }]).flat();
};

Gtk.StringList.prototype[$getChildren] = function() {
    return listModelToArray(this).map(object => {
        object[$removeChild] = () => {
            let position = listModelToArray(this).findIndex(item => item == object);
            if (position != -1)
                this.remove(position);
        };

        return { object, ignoreUI: true };
    });
};

Gtk.TextTagTable.prototype[$getChildren] = function() {
    let tags = [];
    this.foreach(tag => {
        tag[$removeChild] = () => this.remove(tag);
        tags.push(tag);
    });

    return tags.map(tag => ({ object: tag, type: "tag" }));
};

Gio.ListStore.prototype[$getChildren] = function() {
    return [...this].map(object => {
        object[$removeChild] = () => {
            let [found, position] = this.find(object);
            if (found)
                this.remove(position);
        };

        return { object };
    });
};

if (Adw) {
    Adw.ActionRow.prototype[$getChildren] = function() {
        let prefixes = findBuildableWidget([this], 'prefixes');
        let suffixes = findBuildableWidget([this], 'suffixes');
        let children = [];

        if (prefixes)
            [...prefixes].forEach(widget => {
                widget[$removeChild] = () => this.remove(widget);
                children.push({ object: widget, type: 'prefix' });
            });

        if (suffixes)
            [...suffixes].forEach(widget => {
                widget[$removeChild] = () => this.remove(widget);
                children.push({ object: widget, type: 'suffix' });
            });

        return children;
    };

    // Not to return the header bar inherited from GtkWindow.
    Adw.ApplicationWindow.prototype[$getChildren] = function() {
        return [this[$getChild]()];
    };

    Adw.Carousel.prototype[$getChildren] = function() {
        return Array.from({ length: this.get_n_pages() }, (e_, index) => {
            let page = this.get_nth_page(index);
            page[$removeChild] = () => this.remove(page);
            return { object: page };
        });
    };

    // Same as Adw.ActionRow but skip the two first suffixes.
    Adw.ComboRow.prototype[$getChildren] = function() {
        let prefixes = findBuildableWidget([this], 'prefixes');
        let suffixes = findBuildableWidget([this], 'suffixes');
        let children = [{ object: this.get_model(), ignoreUI: true }];

        if (prefixes)
            [...prefixes].forEach(widget => {
                widget[$removeChild] = () => this.remove(widget);
                children.push({ object: widget, type: 'prefix' });
            });

        if (suffixes)
            [...suffixes].slice(2).forEach(widget => {
                widget[$removeChild] = () => this.remove(widget);
                children.push({ object: widget, type: 'suffix' });
            });

        return children;
    };

    Adw.ExpanderRow.prototype[$getChildren] = function() {
        // ExpanderRow add a box to the "prefixes" box of its internal ActionRow.
        let prefixes = findBuildableWidget([this], 'prefixes')?.get_first_child();
        let actions = findBuildableWidget([this], 'actions');
        let list = findBuildableWidget([this], 'list');
        let children = [];

        if (prefixes)
            [...prefixes].forEach(widget => {
                widget[$removeChild] = () => this.remove(widget);
                children.push({ object: widget, type: 'prefix' });
            });

        if (actions)
            [...actions].forEach(widget => {
                widget[$removeChild] = () => this.remove(widget);
                children.push({ object: widget, type: 'action' });
            });

        if (list)
            [...list].forEach(row => {
                let widget = row instanceof Adw.PreferencesRow ? row : (row.child ?? row);
                widget[$removeChild] = () => this.remove(row);
                children.push({ object: widget });
            });

        return children;
    };

    Adw.Flap.prototype[$getChildren] = function() {
        return ['content', 'flap', 'separator'].map(prop => {
            let widget = this[prop];
            if (widget)
                widget[$removeChild] = () => (this[prop] = null);

            return { object: widget, type: prop };
        });
    };

    Adw.HeaderBar.prototype[$getChildren] = function() {
        let children = [];
        let centerBox = this.get_first_child().child;

        if (centerBox.get_start_widget()) {
            // Comparatively to GtkHeaderBar, AdwHeaderBar add an intermediate gizmo
            // to the start widget.
            [...centerBox.get_start_widget().get_first_child()].forEach(widget => {
                if (widget instanceof Gtk.WindowControls)
                    return;

                widget[$removeChild] = () => this.remove(widget);
                children.push({ object: widget, type: 'start' });
            });
        }

        if (this.get_title_widget()) {
            this.titleWidget[$removeChild] = () => this.set_title_widget(null);
            children.push({ object: this.titleWidget, type: 'title' });
        }

        if (centerBox.get_end_widget()) {
            // Comparatively to GtkHeaderBar, AdwHeaderBar add an intermediate gizmo
            // to the end widget.
            [...centerBox.get_end_widget().get_first_child()].forEach(widget => {
                if (widget instanceof Gtk.WindowControls)
                    return;

                widget[$removeChild] = () => this.remove(widget);
                children.push({ object: widget, type: 'end' });
            });
        }

        return children;
    };

    Adw.Leaflet.prototype[$getChildren] = Gtk.Stack.prototype[$getChildren];

    Adw.PreferencesGroup.prototype[$getChildren] = function() {
        let listbox = findBuildableWidget([this], 'listbox');
        if (!listbox)
            return [];

        return [...listbox].map(row => {
            row[$removeChild] = () => this.remove(row);
            return { object: row };
        });
    };

    Adw.PreferencesPage.prototype[$getChildren] = function() {
        let box = findBuildableWidget([this], 'box');
        if (!box)
            return [];

        return [...box].map(group => {
            group[$removeChild] = () => this.remove(group);
            return { object: group };
        });
    };

    Adw.PreferencesWindow.prototype[$getChildren] = function() {
        let pagesStack = findBuildableWidget([this], 'pages_stack');
        if (!pagesStack)
            return [];

        let list = pagesStack.get_pages();
        let pages = listModelToArray(list);

        // ⚠️: page is a GtkPage and page.child is an AdwPreferencesPage.
        // The same as for GtkStack, but ignore UI for page instead of page child.
        return pages.map(page => {
            page[$removeChild] = () => this.remove(page.child);
            page.child[$removeChild] = page[$removeChild];
            return [{ object: page, ignoreUI: true }, { object: page.child }];
        }).concat([{ object: list, ignoreUI: true }]).flat();
    };

    Adw.Squeezer.prototype[$getChildren] = Gtk.Stack.prototype[$getChildren];

    // Contrary to other stack-like widget classes,
    // AdwTabView does not support adding children via GtkBuilder.
    Adw.TabView.prototype[$getChildren] = function() {
        let list = this.get_pages();
        let pages = listModelToArray(list);

        return pages.map(page => {
            page[$removeChild] = () => this.close_page(page);
            page.child[$removeChild] = page[$removeChild];
            return [{ object: page, ignoreUI: true }, { object: page.child, ignoreUI: true }];
        }).concat([{ object: list, ignoreUI: true }]).flat();
    };

    // Not to return the header bar inherited from GtkWindow.
    Adw.Window.prototype[$getChildren] = function() {
        return [this[$getChild]()];
    };
}

export const getChildren = function(object) {
    let children = object[$getChildren]().filter(child => !!child.object);

    if ((object instanceof Gtk.Widget) && object.layoutManager)
        children.push({ object: object.layoutManager, ignoreUI: true });

    return children;
};

export const canRemove = function(object) {
    return !!object[$removeChild];
};

export const removeChild = function(object) {
    object[$removeChild]();
    delete object[$removeChild];
};
