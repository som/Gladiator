/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { GObject } = imports.gi;

const LIBRARIES = ['Adw', 'Gdk', 'Gio', 'Gsk', 'Gtk', 'Pango'];

const libClasses = LIBRARIES
    .map(libName => {
        let Lib;
        try {
            Lib = imports.gi[libName];
        } catch(e) {
            return [];
        }

        return Object.keys(Lib)
            .filter(className => {
                try {
                    return !!Lib[className]?.$gtype;
                } catch(e) {
                    return false;
                }
            })
            .map(className => Lib[className]);
    })
    .flat();

let initialClasses = [];
let classes;

GObject.registerClassOld = GObject.registerClass;
GObject.registerClass = function(...args) {
    let Klass = GObject.registerClassOld(...args);

    if (classes) {
        if (GObject.type_is_a(Klass.$gtype, GObject.TYPE_ENUM))
            classes.enum.push(Klass);
        else if (GObject.type_is_a(Klass.$gtype, GObject.TYPE_FLAGS))
            classes.flags.push(Klass);
        else if (
            GObject.type_is_a(Klass.$gtype, GObject.TYPE_OBJECT) &&
            !GObject.type_is_a(Klass.$gtype, GObject.TYPE_INTERFACE)
        )
            classes.object.push(Klass);
    }

    return Klass;
};

function getClasses() {
    if (!classes) {
        let allClasses = initialClasses.concat(libClasses);

        classes = {
            enum: allClasses.filter(Klass =>
                GObject.type_is_a(Klass.$gtype, GObject.TYPE_ENUM)
            ),
            flags: allClasses.filter(Klass =>
                GObject.type_is_a(Klass.$gtype, GObject.TYPE_FLAGS)
            ),
            object: allClasses.filter(Klass => (
                GObject.type_is_a(Klass.$gtype, GObject.TYPE_OBJECT) &&
                !GObject.type_is_a(Klass.$gtype, GObject.TYPE_INTERFACE) &&
                !GObject.type_test_flags(Klass.$gtype, GObject.TypeFlags.ABSTRACT)
            )),
        };
    }

    return classes;
}

export function getEnumClasses() {
    return getClasses().enum;
}

export function getFlagsClasses() {
    return getClasses().flags;
}

export function getObjectClasses() {
    return getClasses().object;
}

export function getPrototype(typeName) {
    return libClasses.find(Klass => Klass.$gtype.name == typeName)?.prototype ?? null;
}

export function setCustomClasses(customClasses) {
    initialClasses = customClasses;
}
