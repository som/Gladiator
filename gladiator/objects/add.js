/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GObject, Gtk } = imports.gi;
const ByteArray = imports.byteArray;
import { getPrototype } from './classes.js';
import './overrides.js';

let Adw;
try {
    Adw = imports.gi.Adw;
} catch(e) {}

const $getFunctions = Symbol('getFunctions');

function getWidgetsForType(widgets, Klass) {
    if (!widgets.length)
        return [];

    let typedChildren = widgets.filter(widget => widget instanceof Klass);

    return typedChildren.concat(
        getWidgetsForType(widgets.map(widget => [...widget]).flat(), Klass)
    );
}

GObject.Object.prototype[$getFunctions] = function() {
    return [];
};

Gtk.Widget.prototype[$getFunctions] = function() {
    return this.set_child ? [{ name: 'set_child', isSetter: true, getterName: 'get_child', params: [{ name: "child" }] }] : [];
};

let dataFile = Gio.File.new_for_uri(import.meta.url).get_parent().get_child('add.json');
let data = JSON.parse(ByteArray.toString(dataFile.load_contents(null)[1]));

Object.entries(data).forEach(([typeName, functions]) => {
    let prototype = getPrototype(typeName);
    if (!prototype)
        return;

    // Skip comments.
    functions = functions.filter(func => typeof func != 'string');

    prototype[$getFunctions] = function() {
        return Object.getPrototypeOf(prototype)[$getFunctions].call(this).concat(functions);
    };
});

// Skip inherited functions.
{
    Gtk.ShortcutsGroup.prototype[$getFunctions] = function() {
        return [{
            name: "add_shortcut",
            params: [
                { name: "shortcut", types: [Gtk.ShortcutsShortcut.$gtype.name] },
            ],
        }];
    };

    Gtk.ShortcutsSection.prototype[$getFunctions] = function() {
        return [{
            name: "add_group",
            params: [
                { name: "group", types: [Gtk.ShortcutsGroup.$gtype.name] },
            ],
        }];
    };

    Gtk.ShortcutsWindow.prototype[$getFunctions] = function() {
        return [{
            name: "add_section",
            params: [
                { name: "section_name", text: true }, { name: "title", text: true },
            ],
        }];
    };
}

Gio.ListStore.prototype[$getFunctions] = function() {
    return [{
        name: "append",
        params: [
            { name: "item", types: [this.get_item_type().name] },
        ],
    }, {
        name: "insert",
        params: [
            { name: "position", numeric: true, lower: 0, upper: this.get_n_items(), value: 0 },
            { name: "item", types: [this.get_item_type().name] },
        ],
    }];
};

if (Adw) {
    let ActionRowGetFunctionsOld = Adw.ActionRow.prototype[$getFunctions];
    Adw.ActionRow.prototype[$getFunctions] = function() {
        let functions = ActionRowGetFunctionsOld.call(this);

        let widgets = [...this.child];
        let activatableWidgets = [];

        while (widgets.length) {
            activatableWidgets.push(
                ...widgets.filter(widget => !!Gtk.Widget.get_activate_signal.call(widget.constructor))
            );

            widgets = widgets.map(widget => [...widget]).flat();
        }

        functions.push({
            name: "set_activatable_widget",
            params: [
                { name: "widget", instances: activatableWidgets, nullable: true },
            ],
        });

        return functions;
    };

    let tabBarGetFunctionsOld = Adw.TabBar.prototype[$getFunctions];
    Adw.TabBar.prototype[$getFunctions] = function() {
        let functions = tabBarGetFunctionsOld.call(this);
        let views = getWidgetsForType([this.root], Adw.TabView);

        functions.push({
            name: "set_view",
            params: [
                { name: "view", instances: views, nullable: true },
            ],
        });

        return functions;
    };
}

export const getAddFunctions = function(object) {
    return object[$getFunctions]()
        .filter(func => !func.isSetter || (func.isSetter && !object[func.getterName]?.()));
};
